title: "GitLab Pages vs. GitHub Pages"
pdf: gitlab-pages-vs-github-pages.pdf
competitor_one:
  name: 'GitLab Pages'
  logo: '/images/comparison/gitlab-logo.svg'
competitor_two:
  name: 'GitHub Pages'
  logo: '/images/comparison/github-logo.svg'
features:
  - title: "Publish static websites for free"
    description: "With both GitLab Pages and GitHub Pages, you can publish a static website for free."
    competitor_one: true
    competitor_two: true
    link_description: "Visit GitLab Pages' webpage for an overview."
    link: '../features/pages/'
  - title: "Publish your website from a private project for free"
    description: "With GitLab Pages, you can create a private repository to hold your site content, and keep only the page source (HTML) available online. With GitHub Pages, you can  do it only if you have a paid subscription."
    competitor_one: true
    competitor_two: false
    link_description: "Visit GitLab Pages Documentation."
    link: 'https://docs.gitlab.com/ce/user/project/pages/'
  - title: "Support dynamic websites"
    description: "With either GitLab or GitHub Pages, you can only publish static websites; dynamic websites are not supported."
    competitor_one: false
    competitor_two: false
    link_description: "Read through an overview on static vs dynamic websites."
    link: '/2016/06/03/ssg-overview-gitlab-pages-part-1-dynamic-x-static/'
  - title: "Built-in Continuous Integration"
    description: |
                  <a href="/features/gitlab-ci-cd/" target="_blank">GitLab CI</a>, built-in GitLab, is the basis for building your website with GitLab Pages. Use GitLab CI to build, test, and deploy your website or webapp continuously. 
    competitor_one: true
    competitor_two: false
    link_description: "Learn how to publish your website with continuous methods."
    link: 'https://docs.gitlab.com/ce/user/project/pages/getting_started_part_four.html#creating-and-tweaking-gitlab-ci-yml-for-gitlab-pages'
  - title: "Custom domains"
    description: "With both GitLab Pages and GitHub Pages websites, your can use a custom domain or subdomain."
    competitor_one: true
    competitor_two: true
    link_description: "Learn how to apply custom domains to GitLab Pages websites."
    link: 'https://docs.gitlab.com/ce/user/project/pages/getting_started_part_three.html#setting-up-custom-domains-dns-records-and-ssl-tls-certificates'
  - title: "Multiple custom domains"
    description: "GitLab Pages allows you to add as many custom domains (known as domain aliases) pointing to a single website. A domain alias is like having multiple front doors to one location."
    competitor_one: true
    competitor_two: false
    link_description: "Learn how to add custom domains to your GitLab Pages website."
    link: 'https://docs.gitlab.com/ce/user/project/pages/introduction.html#add-a-custom-domain-to-your-pages-website'
  - title: "Secure custom domains (HTTPS)"
    description: "Install a SSL/TLS certificate, at no cost, on a website set up with a custom domain served by GitLab Pages."
    competitor_one: true
    competitor_two: false
    link_description: "Learn how to add an SSL/TLS certificate to your GitLab Pages website."
    link: 'https://docs.gitlab.com/ce/user/project/pages/getting_started_part_three.html#ssl-tls-certificates'
  - title: "Build any Static Site Generator"
    description: |
                  GitHub Pages is integrated with <a href="https://jekyllrb.com" target="_blank" rel="nofollow">Jekyll</a>, therefore, it's the only SSG it builds. With GitLab, you can build any SSG, and also choose specific SSGs' versions you want your site to build with (e.g. Middleman 4.1.1).
    competitor_one: true
    competitor_two: false
    link_description: "Learn how to build any SSG with GitLab Pages."
    link: '/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/'
  - title: "Example projects"
    description: |
                  With GitHub Pages, you can choose one in a few Jekyll themes get you started. Similarly, with GitLab Pages, you can choose an <a href="https://gitlab.com/pages" target="_blank">example project</a> to fork and get started with.
    competitor_one: true
    competitor_two: true
    link: 'https://www.youtube.com/watch?v=TWqh9MtT4Bg'
    link_description: "Watch a 3-min video tutorial on how to get started with GitLab Pages by forking a project."
  - title: "All Jekyll plugins"
    description: |
                  Besides building any Jekyll version you want, with GitLab Pages you can use <a href="https://jekyllrb.com/docs/plugins/" rel="nofollow" target="_blank">all Jekyll plugins available</a>. GitHub Pages allows you to use <a href="https://help.github.com/articles/adding-jekyll-plugins-to-a-github-pages-site/" rel="nofollow" target="_blank">only a few plugins</a>.
    competitor_one: true
    competitor_two: false
  - title: "Custom Error Pages (404)"
    description: "Both GitLab Pages and GitHub Pages allow you to create custom error pages."
    competitor_one: true
    competitor_two: true
    link_description: "Learn how to add a custom 404 to GitLab Pages websites."
    link: 'https://docs.gitlab.com/ee/user/project/pages/introduction.html#custom-error-codes-pages'

