---
layout: markdown_page
title: "Sales Development"
---

---
## On this page
{:.no_toc}

- TOC
{:toc}

---

## Job Description

As a Sales Development Representative (SDR) you are focused on driving net new business for the company. We thrive on competition and our ability to cold call/email. You will be aligned with Account Executives (AE’s) to work a strategic list of 50-100 targeted accounts on a quarterly basis..

## Expectations

You will be expected to:
* Build a trusting relationship with your assigned AE’s
* Hit your monthly goals. Which are opportunities/SQLs (Sales Qualified Leads) generated
* Have a good balance of phone calls made and emails sent (there are no daily metric requirements around this)
* Complete the GitLab beginner, intermediate, and expert certification courses
* Be able to demo GitLab to one of the sales directors

## Working with AE’s

A good portions  of your job requires you to work with assigned AE’s. This is key to our role as SDRs’ because they will not only help you create an appropriate strategy to go after specific accounts, but they will help you build your career as a future AE for GitLab. It is crucial that you build a relationship of honesty and trust. 

### Account Distribution

* When you are assigned to your AE’s you will work with them to create a list of accounts for you to go after. This should consist of ~50-100 Large or Strategic accounts to work in a given quarter. 
* In the situation that you do not have assigned AE’s, your Team Lead will provide you with a list of unnamed top strategic or large accounts to go after. (When you create meetings you will need to coordinate with your Team Lead to decide what AE that account will go to. You will also work with your Team Lead to develop strategy for your assigned accounts).
* In the event that you feel your accounts are non-workable please consult with your Team Lead.
* We use this [report](https://na34.salesforce.com/00O61000003iZLP) to track and ensure proper coverage of stategic and large accounts and effective use of SDR resources.  

### Account Management

* We use SalesForce as our CRM to manage accounts. 
* When it comes to task management there are a couple of different ways to do so. You can use either SFDC (SalesForce.com) or you can use Outreach which is primarily used for emailing. 

### SDR/AE Relationship

* Communication is priority in our company. You will need to create a working relationship with your assigned AE’s. 
* The relationship between you and your AE’s should be one of trust, honesty, and openness. It is important to understand that this relationship is a two-way street. The SDR and the AE will need to put in equal amounts of effort to make this relationship work. When the relationship works, both the SDR and AE become successful. 
* There should be, at minimum, a weekly 1:1 with each your AE’s to discuss:
* Where you are at for the month, metric wise.
* What you have done with the accounts you are working. (This will be what is working well, what you have found out about company structure, who are the right people to talk to, and meetings/opportunities you have created.)
* Discuss evolving strategy for “currently working” accounts and “new accounts” you will be going after. 

## Metrics

* SDR’s compensation is based on 1 key metrics:
   * Opportunities created (SQL’s)

* SDRs’ will also be measured on, but not tied to their compensation:
    * Number of contacts added to named account
    * Account Mapping
    * roles identified for each contact
    * software development tools being used
    * how the organization is set up - who are the other teams that we can expand into
    * Purchasing process - do they have a procurement team, do/can they use a fulfillment agency
    * % of named accounts contacted
    * Number of contacts with activity (email and/or calls)
    * Number of emails sent
    * Number of calls made
    * Number of connects (calls between SDR and prospect)
    * Number of sales appointments (calls between prospect, AE and SDR to explore if there is an opportunity)
    * Pipeline of SDR sourced opportunities
    * ACV won from opportunities SDR sources. 

## Criteria for an SQL
 
 1. Authority 
  * What role does this person play in the decision process? (i.e. decision maker, influencer, etc.)
  * OR is this someone that has a pain and a clear path to the decision maker

 2. Need
  * The buyer has either a defined project or a pain has been identified
  
 3. Required Information
  * Number of potential EE users on this opp
  * Current technologies and when the contract is up
  * Current business initiatives

 4. Handoff
  * The buyer is willing to meet with an AE

## Criteria for Sales Accepted Opportunity (SAO)

 1. SQL Definition Met
  * AE confirmed authority and need
  * AE confirms number of potential EE users
  * AE confirms that the SQL information is logged into SFDC

 2. Handoff
  * The SDR has made an introduction with the AE to the prospect via email
  * SDR schedules a calendar invite with buyer and AE
  * The SDR confirms the agenda and set expections for the phone call

 3. Meeting Completed
  * The AE conducts the first meeting via phone (it is recommended that the SDR is on the phone call as well)
  * The AE re-confirms the information passed by the SDR

 4. Next Step Identified
  * The AE schedules a next event with the AE (typically another phone call to dive in deeper to the clients pains and needs)
  * The AE has 1 business day to accept/reject the opportunity

## Ramping for new hires

Month 1:
 
 * Daily/Weekly/Monthly Activity [40/200/600]
 * Daily/Weekly/Monthly Emails [20/100/400]
 * Daily/Weekly/Monthly Calls [10/50/200]
 * % of named accounts contacted (monthly) [25%]
 * Average # of contacts per named account contacted (monthly) [5]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [1]
 * Connects (weekly/monthly) - calls SDR’s are having [5/15]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [3]
 * % of named accounts with sales appointment
 * Opportunities created (weekly/monthly) [1] -If the SDR starts on or before halfway through a month. If the SDR start after halfway through the month they carry a SQL quota of 0 for that month.
 * % of named accounts with an opportunity
 * ACV won from sourced opportunities (monthly/quarterly) [$0]

Month 2:
 
 * Daily/Weekly/Monthly Activity [60/300/1200]
 * Daily/Weekly/Monthly Emails [40/200/800]
 * Daily/Weekly/Monthly Calls [20/100/400]
 * % of named accounts contacted (monthly) [50%]
 * Average # of contacts per named account contacted (monthly) [10]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [3]
 * Connects (weekly/monthly) - calls SDR’s are having [10/40]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [3/12]
 * % of named accounts with sales appointment
 * Opportunities created (weekly/monthly) [1/4] 
 * % of named accounts with an opportunity
 * ACV won from sourced opportunities (monthly/quarterly) [$0] 

Month 3+:

 * Daily/Weekly/Monthly Activity [90/450/1800]
 * Daily/Weekly/Monthly Emails [60/300/1200]
 * Daily/Weekly/Monthly Calls [30/150/600]
 * % of named accounts contacted (monthly) [75%]
 * Average # of contacts per named account contacted (monthly) [15]
 * Decision Makers confirmed/added (weekly) - persons they identify in their research and calls not from discover.org.  Who makes the decision for software development tools (i.e. purchased GitHub, Jenkins, JIRA) [4]
 * Connects (weekly/monthly) - calls SDR’s are having [15/60]
 * Sales Appointments (weekly/monthly) - calls SDR’s and AE’s are having with prospect (influencer or Decision Maker) [5/20]
 * % of named accounts with sales appointment
 * Opportunities created (weekly/monthly) [2.5/8]
 * % of named accounts with an opportunity
 * ACV won from sourced opportunities (monthly) [$80,000] 

## How to create an opportunity

Here are the steps for you to create an SDR opportunity along with SFDC hygiene when it comes to your opps. We want to make sure that it is very clear on what is going on with every opportunity and it is going to be up to you and your AE to make sure that everything is accurate within each opportunity you create.

Steps:
### Where to find the opportunity button

![optional alt text](new/opportunity_button.png)

* You can find the “New Opportunity” button (A) about three-quarters of the way down on the account page (B) as shown above.

### Channel or Standard

![optional alt text](standard/opp_button.png)

* Always select “standard” (A) when creating an opp. Then click “Continue” to go the opp screen (B). Channel is more for the channel managers that we have working with vendors.

### Opportunity Information

![optional alt text](Opportunity_Information.png)

##### A. Naming an opp
* [Name of Company]- [Quantity] [Abbreviations of Product]
* Example: Acme, Inc- 50 EES
* Example: Acme, Inc- 50 EEP

##### B. Account name
* Always double check to make sure that the account name is accurate for which you are creating the opp for. 

##### C. Type 
* Here is where you are going to mark what type of opportunity this is. As an SDR the type should be one of 3, New Business, Add-On Business, or Existing Customer - Cross-Sell.  
 * New Business - This type should be used for any new account (business) who signs up either through the sales team or via the web portal. Paid training also falls under this type if the organization does not have an enterprise license.
 * Add-on Business- This type should be used for any incremental/upsell business sold into an existing account and division mid term, meaning not at renewal.
 * Existing Account - Cross-Sell- This type should be used for new business sold into an existing account but a new division, a new purchasing group.

##### D. Lead Source
* The lead source is where the lead came from. This is important for the company so we can keep track of where are leads/prospects are coming from. MAKE SURE THAT YOU MARK THIS ACCURATELY. If you are not sure what to mark it as, then you will need to talk with your manager.

##### E. SDR
* Here is where you will mark yourself as the SDR working this opp. 

##### F. Close Date
* This part is a bit more tricky than the others. Try to be as accurate as possible with this field as it will be determining when the opp will close. If you can ask a question around this. Such as, If you were to purchase when would you expect to start migrating people over? Or When evaluating a tool like this, how long would you say your buying process is? If you can’t get this information, you would be safe to put it 6 months out. Or, when is your contract up for the system you are evaluating to replace (GitHub, BitBucket, Subversion, etc.) 

##### G. Stage
* As an SDR, whenever you create a new opp all of your opportunities should be in the __**BDR qualified stage**__. It will then be up to your AE to move the opp to the next stages. Note that you do not get paid on BDR qualified opps. So you will want to have conversations with your AE to make sure that the opp is moving forward. Once the AE moves the opp to the next stage (which is typically “Discovery”) the AE will then need to put in a “Sales Qualified Date” and check the Sales Qualified box as shown below.

![SQD/SQ](SQD/SQ.png)

##### H. Amount
* In your “qualification” process it will be on you as the SDR to ask a question about how many users are going to be on this opportunity (look at the SDR playbook for different ways to ask this question). After you get this information you then will multiply the number of users by $199.99 to get your amount. We assume that all of the accounts we are working will be purchasing our EE premium product.

### Qualification Questions

![optional alt text](Qualifications_Questions.png)

##### A. Prospect Indetified Need
* This is a key piece of information to know. You will need to identify a pain or need that we can solve in order to create an opp. Make sure to have this information.

##### B. What is Prospect Doing to Address Need
* This will be nice to know, but not a crucial piece of information to know in order to create an opp. But this should be easy enough to get and should be common practice to get this information

##### C. Is There a Budget Secured for Project
* Again this is good to know but not absolutely necessary to create an opp.

##### D. Who is the Decision Maker for GitLab
* This is asking from the prospect side for who makes the decision when it comes to purchasing this type of tool. Good to know, but not absolutely necessary to create an opp.

##### E. Buying Process for Procurring GitLab
* This is asking what is the process on the prospects end to purchase a tool like GitLab. What process do they need to go through to purchase GitLab.

##### F. Role Prospect Plays in Evaluation
* Simple enough to mark what role the prospect plays in the buying process. I would recommend that you do your best as an SDR to have conversations with Decision Makers, Economic Buyer, Influencer, Economic Decision Maker, and Evaluators. 

##### G. Qualification Notes
* This is an important part of Opportunity hygiene. You will need to make sure to put in any notes from conversations that you have with the prospect. That even includes conversations prior to creating the opp as well. This will help the AE to understand what has been talked about already so they do not have to repeat any unnecessary information or questions that have already been talked about. **SO MAKE SURE YOU TAKE GOOD NOTES**. 

##### H. What is the Timeline to Make the Decision
* Here we are looking to talk to people that are making a decision within 12 months. Easy question to ask and straightforward.

##### I. Interested in GitLab EE
* This is a no-brainer. Make sure that the people you are talking to are interested in GitLab EE. Since these are the only people we should be talking to, this should be standard information to gather when qualifying your prospect.

##### J. Using Another Version of GitLab
* This should be done in your research portion before reaching out to any prospect. Use version.gitlab or legacy.gitlab to find out if that company is using any version of GitLab.

##### K. How Many Seats are They Interested in
* This is crucial piece of information that you need to gather in your qualification call. This will not only determine how many users will be on this opp, but this will also determine the SQL amount from above that we talked about. You need to make sure that this is as accurate as possible.

##### L. Meeting with an AE to Discuss Next Steps
* If you are creating an opp this better be marked as “yes” and better be true.

##### M. Competitors
* Finally, if you know of any competitors they are using, put them here so the AE knows who they are using so they can speak appropriately to our differentiators. 


### Salesforce Hygiene for your opportunites 

[SDR sourced opps](https://na34.salesforce.com/00O61000003iXGw) This is the report that the leadership uses to see all the opps that have been created from the SDR team. I strongly recommend that you live in this report for the opps that you are creating. If you make sure that all the above information is as accurate as possible it will help you to not be under the microscope from your manager and the executive team. 

I highly recommend also to stay on top of your AE’s to make sure they are progressing the opportunity from BDR qualified to Discovery stage to make sure you get paid on the opps that you are creating. All of the above steps and processes are only going to help you create healthy habits for when you become an AE. 

It will be in your best interest to also sit in on as many meetings as possible with your different AE’s and at different stages in the buying process to see how the AE’s work with prospects beyond qualifying. The idea is to create a habits for success. 
